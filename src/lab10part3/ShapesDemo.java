/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package lab10part3;
import static org.junit.Assert.*;
/**
 *
 * @author jaiminlakhani
 */
public class ShapesDemo {
    
    public static void calculateArea( Rectangle r ) {
        r.setHeight(3);
        r.setWidth(2);
        
        assertEquals(" Area calculation is incorrect ", r.getArea(), 10);
    }
    public static void main(String[] args) {
       ShapesDemo.calculateArea (new Rectangle (10, 20));
        
       ShapesDemo.calculateArea (new Square ());       
    }
}